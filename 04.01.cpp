﻿#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <boost/algorithm/string/trim.hpp>
#include <optional>
#include <Windows.h>
using namespace std;

struct Item
{
    string key;
    string data;
};

struct Table
{
    fstream connection;
    string tableName = "table";
    vector<Item> data;
};

struct TableLocalization
{
    struct Menu {
        string write;
        string read;
        string del;
        string close;
    }menu;

    struct Input {
        string wrongInput;
    }input;

    struct Status {
        string error;
    }status;

    struct Write
    {
        string change;
        string addNew;
    }menuWrite;

    struct Elements
    {
        string chooseItem;
        string inputNewItemName;
        string inputNewItemData;
        string noItemsFound;
        string itemValue;
        string ItemKeyAlreadyExists;
    }actionInformation;
};

enum class Menu
{
    Write = 1,
    Read,
    Delete,
    Close
};

enum class MenuWrite
{
    Change = 1,
    AddNew
};

enum class ErrorCode
{
    CantOpenFile,
    FileIsEmpty,
    NoItemsFound
};

template<class T>
size_t getCountOfRows(T& table, const TableLocalization& tableLocalization) noexcept(false)
{
    if (!table.connection.is_open())
    {
        throw ErrorCode::CantOpenFile;
    }

    size_t rows = 0;
    string line;
    while (getline(table.connection, line))
    {
        rows++;
    }

    table.connection.clear();
    table.connection.seekp(0, ios_base::beg);
    return rows;
}

string getItemValue(Table& table, string key) noexcept(false)
{
    for (const auto& el : table.data)
    {
        if (el.key == key)
        {
            return el.data;
        }
    }

    throw ErrorCode::NoItemsFound;
}

void serializetion(Table& table, const TableLocalization& tableLocalization) noexcept(false)
{
    table.connection.open(table.tableName, ios::in | ios::binary);
    if (!table.connection.is_open())
    {
        table.connection.open(table.tableName, ios::out | ios::binary);
        table.connection.close();

        table.connection.open(table.tableName, ios::in | ios::binary);
        if (!table.connection.is_open())
        {
            throw ErrorCode::CantOpenFile;
        }
    }

    auto rows = getCountOfRows(table, tableLocalization);
    for (size_t i = 0; i < rows; i++)
    {
        Item temp{};
        getline(table.connection, temp.key, ':');
        getline(table.connection, temp.data);
        boost::trim(temp.data);
        boost::trim(temp.key);
        table.data.push_back(temp);
    }

    table.connection.close();
}

void localization(TableLocalization& tableLocalization) noexcept(false)
{
    Table temp;
    temp.tableName = "localization.ini";
    serializetion(temp, tableLocalization);
    if (temp.data.size() == 0)
        throw ErrorCode::FileIsEmpty;

    tableLocalization.input.wrongInput = getItemValue(temp, "inputWrongInput");
    tableLocalization.status.error = getItemValue(temp, "statusError");
    tableLocalization.menu.write = getItemValue(temp, "menuWrite");
    tableLocalization.menu.read = getItemValue(temp, "menuRead");
    tableLocalization.menu.del = getItemValue(temp, "menuDelete");
    tableLocalization.menu.close = getItemValue(temp, "menuClose");
    tableLocalization.menuWrite.change = getItemValue(temp, "menuWriteChange");
    tableLocalization.menuWrite.addNew = getItemValue(temp, "menuWriteAddNew");
    tableLocalization.actionInformation.chooseItem = getItemValue(temp, "actionInformationChooseItem");
    tableLocalization.actionInformation.inputNewItemName = getItemValue(temp, "actionInformationInputNewItemName");
    tableLocalization.actionInformation.inputNewItemData = getItemValue(temp, "actionInformationInputNewItemData");
    tableLocalization.actionInformation.noItemsFound = getItemValue(temp, "actionInformationNoItemsFound");
    tableLocalization.actionInformation.itemValue = getItemValue(temp, "actionInformationItemValue");
    tableLocalization.actionInformation.ItemKeyAlreadyExists = getItemValue(temp, "actionInformationItemKeyAlreadyExists");
}

void deserializetion(Table& table) noexcept(false)
{
    table.connection.open(table.tableName, ios::out | ios::binary);
    if (!table.connection.is_open())
    {
        throw ErrorCode::CantOpenFile;
    }

    for (size_t i = 0; i < table.data.size(); i++)
    {
        table.connection.write(table.data[i].key.c_str(), table.data[i].key.size());
        table.connection.write(":", sizeof(char));
        table.connection.write(table.data[i].data.c_str(), table.data[i].data.size());
        table.connection << endl;
    }

    table.connection.close();
}

void insertItem(Table& table, const Item& item)
{
    table.data.push_back(item);
}

optional<Item> searchForItem(Table& table, const string_view key)
{
    for (const auto& el : table.data)
    {
        if (el.key == key)
        {
            return make_optional<Item>(el);
        }
    }
    return {};
}

bool deleteItem(Table& table, const string_view key)
{
    return erase_if(table.data, [&key](const auto& item) 
           {
                return item.key == key;
           } ) != 0u;     
}

bool changeItemData(Table& table, const string_view key, const string_view data) noexcept(false)
{
    auto iter = find_if(table.data.begin(), table.data.end(), [&key](const auto& item)
        {
            return item.key == key;
        });
    if (iter == table.data.end())
        throw ErrorCode::NoItemsFound;

    iter->data = data;
    return true;
}

void outputItem(const Item& item)
{
    cout << item.key << " : " << item.data << endl;
}

void outputAllItems(const Table& table)
{
    for (const auto& el : table.data)
    {
        outputItem(el);
    }
}

bool checkKey(Table& table, const string_view key)
{
    return any_of(table.data.begin(), table.data.end(), [&key](const auto& item)
           {
                return item.key == key;
           });
}

void menuChange(Table& table, const TableLocalization& tableLocalization)
{
    if (table.data.size() == 0)
    {
        cout << tableLocalization.actionInformation.noItemsFound << endl;
        system("pause");
        return;
    }

    string keyTemp;
    cout << tableLocalization.actionInformation.chooseItem << ": ";
    cin.ignore();
    getline(cin, keyTemp);

    auto searchResult = searchForItem(table, keyTemp);
    if (searchResult.has_value())
    {
        string dataTemp;
        cout << tableLocalization.actionInformation.inputNewItemData << ": ";
        getline(cin, dataTemp);

        changeItemData(table, keyTemp, dataTemp);
        return;
    }       

    cout << tableLocalization.actionInformation.noItemsFound << endl;
    system("pause");
}

void menuAddNew(Table& table, const TableLocalization& tableLocalization)
{
    cout << tableLocalization.actionInformation.inputNewItemName << ": ";
    cin.ignore();
    Item newTempItem{};
    getline(cin, newTempItem.key);
    if (checkKey(table, newTempItem.key))
    {
        cout << tableLocalization.actionInformation.ItemKeyAlreadyExists << endl;
        system("pause");
        return;
    }
    cout << tableLocalization.actionInformation.inputNewItemData << ": ";
    getline(cin, newTempItem.data);
    insertItem(table, newTempItem);    
}

void menuRead(Table& table, const TableLocalization& tableLocalization)
{
    if (table.data.size() == 0)
    {
        cout << tableLocalization.actionInformation.noItemsFound << endl;
        system("pause");
        return;
    }

    string keyTemp;
    cout << tableLocalization.actionInformation.chooseItem << ": ";
    cin.ignore();
    getline(cin, keyTemp);

    auto searchResult = searchForItem(table, keyTemp);
    if (searchResult.has_value())
    {        
        cout << tableLocalization.actionInformation.itemValue << ": " 
             << getItemValue(table, keyTemp) << endl;
        system("pause");
        return;       
    }

    cout << tableLocalization.actionInformation.noItemsFound << endl;
    system("pause");
}

void menuDelete(Table& table, const TableLocalization& tableLocalization)
{
    if (table.data.size() == 0)
    {
        cout << tableLocalization.actionInformation.noItemsFound << endl;
        system("pause");
        return;
    }

    string keyTemp;
    cout << tableLocalization.actionInformation.chooseItem << ": ";
    cin.ignore();
    getline(cin, keyTemp);

    auto searchResult = searchForItem(table, keyTemp);
    if (searchResult.has_value())
    {
        deleteItem(table, keyTemp);
        return;
    }

    cout << tableLocalization.actionInformation.noItemsFound << endl;
    system("pause");
}

void menu(Table& table, Menu choice, const TableLocalization& tableLocalization)
{
    if (choice == Menu::Write)
    {
        system("cls");
        int choiceWriteTemp = 0;

        cout << static_cast<size_t>(MenuWrite::Change) << "." << tableLocalization.menuWrite.change << endl;
        cout << static_cast<size_t>(MenuWrite::AddNew) << "." << tableLocalization.menuWrite.addNew << endl;

        cin >> choiceWriteTemp;
        MenuWrite choiceWrite = static_cast<MenuWrite>(choiceWriteTemp);

        if (choiceWrite == MenuWrite::Change)
        {
            system("cls");
            outputAllItems(table);
            menuChange(table, tableLocalization);
        }
        else if (choiceWrite == MenuWrite::AddNew)
        {
            system("cls");
            outputAllItems(table);
            menuAddNew(table, tableLocalization);
        }
    }
    else if (choice == Menu::Read)
    {
        system("cls");
        outputAllItems(table);
        menuRead(table, tableLocalization);
    }
    else if (choice == Menu::Delete)
    {
        system("cls");
        outputAllItems(table);
        menuDelete(table, tableLocalization);
    }
    else if (choice == Menu::Close)
    {
        return;
    }
    else
    {
        cout << tableLocalization.input.wrongInput << endl;
        system("pause");
    }
}

const char* getMessageById(ErrorCode id)
{
    if (id == ErrorCode::CantOpenFile)
        return  "Can't open file";
    if (id == ErrorCode::FileIsEmpty)
        return "Localization file is empty";
    if (id == ErrorCode::NoItemsFound)
        return "No item with such key";
    return "Invalid error code";
}


int main()
{
    try 
    {
        TableLocalization tableLocalization = {};
        Table table = {};
        serializetion(table, tableLocalization);
        localization(tableLocalization);

        int choice = 0;

        do
        {
            system("cls");
            outputAllItems(table);
            cout << static_cast<int>(Menu::Write) << "." << tableLocalization.menu.write << endl;
            cout << static_cast<int>(Menu::Read) << "." << tableLocalization.menu.read << endl;
            cout << static_cast<int>(Menu::Delete) << "." << tableLocalization.menu.del << endl;
            cout << static_cast<int>(Menu::Close) << "." << tableLocalization.menu.close << endl;
            cin >> choice;


            menu(table, static_cast<Menu>(choice), tableLocalization);

        } while (choice != static_cast<int>(Menu::Close));

        deserializetion(table);
    }
    catch(ErrorCode id)
    {
        MessageBoxA(nullptr, getMessageById(id), "Error", MB_OK);
    }
    return 0;
}